import Product from "../Product";

const MenuContainer = ({ props, handleClick }) => {
    return (
        <ul>
            {props.map((item, index) =>
                <Product props={item} handleClick={handleClick} key={index} />
            )}
        </ul>
    )
};
export default MenuContainer;