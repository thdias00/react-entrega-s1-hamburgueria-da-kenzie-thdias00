const ShoppingCart = ({ props, cartTotal, removeAll, removeItem }) => {
    return (
        <>
            {props.length === 0 ? (
                <div className="ShoppingCart-Text">
                    <h3>Sua sacola está vazia</h3>
                    <h4>Adicione itens</h4>
                </div>
            ) : (
                <div className="ShoppingCart-container">
                    {props.map((item, index) =>
                        <div key={index} className="ShoppingCart-items">
                            <img src={item.img} alt={item.name} />
                            <span>{item.name}</span>
                            <p>{item.category}</p>
                            <button onClick={() => removeItem(item)}>Remover</button>
                        </div>
                    )}
                    <span></span>
                    <div className="ShoppingCart-footer">
                        <span>Total</span>
                        <span>R$ {cartTotal.toFixed(2)}</span>
                    </div>
                    <button onClick={removeAll}>Remover Tudo</button>

                </div>
            )}
        </>
    )
};

export default ShoppingCart;