const Product = ({ props, handleClick }) => {
    return (
        <div className='App-items-li'>
            <div className="App-img">
                <img src={props.img} alt={props.name} />
            </div>
            <div className="App-info">
                <li><h3>{props.name}</h3></li>
                <li><span>{props.category}</span></li>
                <li><p>R$ {props.price}</p></li>
                <button onClick={() => handleClick(props)}>Adicionar</button>
            </div>
        </div>
    )
};
export default Product;