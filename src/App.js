import './App.css';
import { useState } from 'react';
import MenuContainer from './components/MenuContainer';
import ShoppingCart from './components/ShoppingCart';

function App() {
  const [products, setProducts] = useState([
    { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 14.00, img: 'https://i.ibb.co/fpVHnZL/hamburguer.png' },
    { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 16.00, img: 'https://i.ibb.co/djbw6LV/x-burgue.png' },
    { id: 3, name: 'Big Kenzie', category: 'Sanduíches', price: 18.00, img: 'https://i.ibb.co/FYBKCwn/big-kenzie.png' },
    { id: 4, name: 'Fanta Guaraná', category: 'Bebidas', price: 5.00, img: 'https://i.ibb.co/cCjqmPM/fanta-guarana.png' },
    { id: 5, name: 'Coca', category: 'Bebidas', price: 4.99, img: 'https://i.ibb.co/fxCGP7k/coca-cola.png' },
    { id: 6, name: 'Fanta', category: 'Bebidas', price: 4.99, img: 'https://i.ibb.co/QNb3DJJ/milkshake-ovomaltine.png' },
  ]);

  const [filteredProducts, setFilteredProducts] = useState([]);
  const [currentSale, setCurrentSale] = useState([]);
  const [cartTotal, setCartTotal] = useState(0);

  const showProducts = (searchProduct) => {
    let newProduct = products.filter((e) => e.category === searchProduct || e.name === searchProduct);
    setFilteredProducts(newProduct);
  };

  const handleClick = (product) => {
    let count = 0;
    const productChange = products.find((e) =>
      e.id === product.id);
    currentSale.forEach((item) => {
      if (item.id === product.id) {
        count++;
      }
    });
    if (count === 0) {
      setCurrentSale([...currentSale, productChange]);
      setCartTotal(cartTotal + product.price);
    }
  };

  const [searchProduct, setSearchProduct] = useState("");

  const removeAll = () => {
    setCurrentSale([]);
    setCartTotal(0);
  };

  const removeItem = (item) => {
    const newCurentSale = currentSale.filter((i) => i !== item);
    setCurrentSale(newCurentSale);
    setCartTotal(cartTotal - item.price);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="App-form">
          <span><h2>Burguer</h2><h4>Kenzie</h4></span>
          <form>
            <input
              type="text"
              value={products.category}
              onChange={(e) => setSearchProduct(e.target.value)}
              placeholder="Digite sua pesquisa"
            />
            <button onClick={() => showProducts(searchProduct)} type="button"><p>Pesquisar</p></button>
          </form>
        </div>
      </header>
      <main>
        <div className='App-items'>
          <MenuContainer props={filteredProducts.length !== 0 ? filteredProducts : products} handleClick={handleClick} />
        </div>
      </main>
      <aside>
        <div className="App-ShoppingCart">
          <div className="ShoppingCart-Title">
            <span>Carrinho de Compras</span>
          </div>
          <ShoppingCart props={currentSale} cartTotal={cartTotal} removeAll={removeAll} removeItem={removeItem} />
        </div>
      </aside>
    </div>
  );
}

export default App;
